import React from "react"
import "./login.css"

const Login = () => {
    return (
        <>
            <div className="page">

                <div className="container">
                          
                    <div className="left-wrap">
                        
                        <div className="text">
                            <h1 className="text-title">got marketing? advance your business insight.</h1>
                            <p className="text-description">Fill out the form and receive our award winning newsletter.</p>
                        </div>
                        
                    </div>                
                    <img className="image" src="img/hands-table-during-business-meeting.jpg" alt="" />
                    
                    <div className="right-wrap">
                        <div className="text-input">
                            <div className="input-button-wrapper">

                                <div className="input-name">
                                    <p className="input-title">Name</p>
                                    <input type="text"/>
                                </div>
                                <div className="input-email">
                                    <p className="input-title">Email</p>
                                    <input type="text"/>
                                </div>                                
                            </div>
                            <button>Sign Me Up</button>
                        </div>                
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login